Here is the jupyter notebook used in our first workshop on 10/6/18.

Try to understand everything and do the "Homework" section at the bottom before next meeting.

There might be bugs because I didn't really double check the code so message me if you find anything

Also hmu if you have questions.

Notice that I ignored the data file (data.csv) using the .gitignore file.  At qis we should always do this to prevent from clogging our repos with giant data files.